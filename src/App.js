import React from 'react';
import FileManager from './Components/FileManager'

function App() {
  return (
    <div className="App">
     <FileManager/>
    </div>
  );
}

export default App;
