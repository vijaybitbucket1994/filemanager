import React, { useState, useEffect, useRef } from "react";
import Modal from "react-modal";
import { data } from "../Data";
import { FaFolder, FaArrowAltCircleLeft } from "react-icons/fa";
import { FiPlus } from "react-icons/fi";
import { FiAlertCircle } from "react-icons/fi";
import { IconContext } from "react-icons";
import "./FileManager.scss";
import _get from "lodash/get";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)"
  }
};
const FileManager = () => {
  const [folder, setFolder] = useState("");
  const [folderContent, setFolderContent] = useState([]);
  const [fileManager, setFileManager] = useState([]);
  const [currentFolder, setCurrentFolder] = useState({
    isRoot: true,
    children: [],
    previousPath: "",
    parent: ""
  });
  const [isError, setIsError] = useState({
    status: false,
    msg: ""
  });
  Modal.setAppElement("body");
  const [modalIsOpen, setIsOpen] = React.useState(false);
  const inputRef = useRef();

  useEffect(() => {
    setFileManager(data);
    let folderNames = Object.keys(data);
    let results = [];
    folderNames.forEach(item => {
      if (item.split("/").length <= 2) {
        results.push({
          name: `/${data[item].name}`,
          path: data[item].path
        });
      }
    });
    setFolderContent(results);
  }, []);

  useEffect(() => {
    if (currentFolder.previousPath !== "") {
      let data = fileManager[`${currentFolder.previousPath}`].children || [];
      setFolderContent(data);
    } else {
      let results = [];
      let folderNames = Object.keys(fileManager);
      folderNames.forEach(item => {
        if (item.split("/").length <= 2) {
          results.push({
            name: `/${fileManager[item].name}`,
            path: fileManager[item].path
          });
        }
      });
      setFolderContent(results);
    }
  }, [fileManager]);


  const openFolder = event => {
    let data = _get(
      fileManager[`${event.currentTarget.dataset.path}`],
      "children",
      []
    );
    setFolderContent(data);
    setCurrentFolder({
      isRoot: false,
      previousPath: `${event.currentTarget.dataset.path}`,
      parent: fileManager[`${event.currentTarget.dataset.path}`].parent,
      currentFolderName:`${event.currentTarget.dataset.name}`.split('/')[1]
    });
  };
  const renderFolders = () => {
    return folderContent.map(item => (
      <div onClick={openFolder} data-path={item.path} data-name={item.name} className="folder-icon">
        <IconContext.Provider value={{ color: "#2196F3", className: "folder" }}>
          <FaFolder />
          <h5 className="folder-label">
            {_get(item, "name", "").split("/")[1]}
          </h5>
        </IconContext.Provider>
      </div>
    ));
  };
  const openModal = async () => {
    await setIsOpen(true);
    await inputRef.current.focus();
  };

  const closeModal = () => {
    setIsOpen(false);
    setFolder("");
    setIsError({
      status: false,
      msg: ""
    });
  };

  const handleChange = event => {
    setIsError({
      status: false,
      msg: ""
    });
    setFolder(event.target.value);
  };

  const handleCreateFolder = () => {
    if (!(folder.length > 3 && folder.length <= 30)) {
      return setIsError({
        status: true,
        msg: "Folder name does not meet the requirement"
      });
    } else {
      let data = fileManager;
      if (!currentFolder.previousPath) {
        if (fileManager.hasOwnProperty(`/${folder}`)) {
          setFolder("");
          return setIsError({
            status: true,
            msg: "Folder already Exist"
          });
        }
        let newFolder = {
          [`/${folder}`]: {
            name: `${folder}`,
            path: `/${folder}`,
            isRoot: true,
            children: [],
            parent: ""
          }
        };

        data = {
          ...fileManager,
          ...newFolder
        };
      } else {
        let isFolderExist = _get(currentFolder, "children", []).includes(
          `/${folder}`
        );
        if (isFolderExist) {
          setFolder("");
          return setIsError({
            status: true,
            msg: "Folder already Exist"
          });
        }
        let newFolder = {
          [`${currentFolder.previousPath}/${folder}`]: {
            name: `${folder}`,
            path: `${currentFolder.previousPath}/${folder}`,
            isRoot: false,
            children: [],
            parent: `${currentFolder.previousPath}`
          }
        };
        let cloneFileManager = { ...fileManager };
        cloneFileManager[`${currentFolder.previousPath}`].children.push({
          name: `/${folder}`,
          path: `${currentFolder.previousPath}/${folder}`
        });
        data = {
          ...cloneFileManager,
          ...newFolder
        };
      }

      setFileManager(data);
      setFolder("");
      closeModal();
    }
  };

  const goBack = () => {
    const path = currentFolder.parent;
    if (!path) {
      let results = [];
      let folderNames = Object.keys(fileManager);
      folderNames.forEach(item => {
        if (item.split("/").length <= 2) {
          results.push({
            name: `/${fileManager[item].name}`,
            path: fileManager[item].path
          });
        }
      });
      setFolderContent(results);
      setCurrentFolder({
        isRoot: true,
        previousPath: "",
        currentFolderName:""
      });
    } else {
      let folderName=path.split('/');
      setFolderContent(_get(fileManager[`${path}`], "children", []));
      setCurrentFolder({
        isRoot: false,
        previousPath: fileManager[`${path}`].parent,
        parent: fileManager[`${path}`].parent,
        currentFolderName:folderName[folderName.length-1]
      });
    }
  };

  return (
    <div className="container">
      {
          !currentFolder.isRoot && (
            <header>
              {currentFolder.currentFolderName}
            </header>
          )
      }
      <div className="folder-container">
        <div className="file-manager">{renderFolders()}</div>
        <button
          title="create folder"
          onClick={openModal}
          className="create-folder-btn"
        >
          <IconContext.Provider
            value={{ color: "greyf", className: "fa-plus" }}
          >
            <FiPlus />
          </IconContext.Provider>
        </button>
      </div>

      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Create Folder"
        shouldCloseOnOverlayClick={false}
      >
        <div className="modal-content">
          <div className="form-group">
            <input
              onChange={handleChange}
              minLength={3}
              type="text"
              className="input-field"
              ref={inputRef}
              value={folder}
              placeholder="Enter Folder Name"
              autofocus
            />
          </div>
          <div className="modal-footer-content">
            <div className="btn-group">
              <button className="btn" onClick={closeModal}>
                Cancel
              </button>
              <button className="btn" onClick={handleCreateFolder}>
                Create Folder
              </button>
            </div>
            <span className="hint-text">
              <IconContext.Provider
                value={{ color: "#FF9800", className: "alert-icon" }}
              >
                <FiAlertCircle />
              </IconContext.Provider>
              Folder name should be less than 30 characters
            </span>
            {isError.status && (
              <span className="error-text">{isError.msg}</span>
            )}
          </div>
        </div>
      </Modal>
      {!currentFolder.isRoot && (
        <footer className="footer">
          <button onClick={goBack}>
            <IconContext.Provider
              value={{ color: "#fff", className: "alert-icon" }}
            >
              <FaArrowAltCircleLeft />
            </IconContext.Provider>
            Previous
          </button>
        </footer>
      )}
    </div>
  );
};

export default FileManager;
