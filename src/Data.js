export const data = {
  "/root": {
    path: "/root",
    isRoot: true,
    name: "root",
    children: [
      {
        name: "/test1",
        path: "/root/test1"
      },
      {
        name: "/test3",
        path: "/root/test3"
      }
    ],
    parent: ""
  },
  "/root/test1": {
    path: "/root/test1",
    isRoot: false,
    parent: "/root",
    name: "test1",
    children: [
      {
        name: "/zig",
        path: "/root/test1/zig"
      }
    ]
  },
  "/root/test3": {
    path: "/root/test3",
    isRoot: false,
    parent: "/root",
    name: "test3",
    children: []
  },
  "/root/test1/zig": {
    path: "root/test1/zig",
    isRoot: false,
    parent: "/root/test1",
    name: "zig",
    children: [
      {
        name: "/tech",
        path: "root/test1/zig/tech"
      }
    ]
  },
  "root/test1/zig/tech": {
    path: "root/test1/zig/tech",
    isRoot: false,
    parent: "/root/test1/zig",
    name: "tech",
    children: []
  },

  "/folder1": {
    path: "/folder1",
    isRoot: true,
    name: "folder1",
    children: [],
    parent: ""
  },
  "/folder2": {
    path: "/folder2",
    isRoot: true,
    name: "folder2",
    children: [],
    parent: ""
  },
  "/folder3": {
    path: "/folder3",
    isRoot: true,
    name: "folder3",
    children: [],
    parent: ""
  },
  "/folder4": {
    path: "/folder4",
    isRoot: true,
    name: "folder4",
    children: [
      {
        name: "/test1",
        path: "/folder4/test1"
      }
    ],
    parent: ""
  },
  "/folder4/test1": {
    path: "/folder4/test1",
    isRoot: true,
    name: "test1",
    children: [],
    parent: "/folder4"
  }
};
